const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
const { default: axios } = require("axios");
const config = require("./config.json");
const fs = require("fs");
const path = require("path");
const runningAt = require("running-at");
// const websocketClient = require("ws");
// const wsClient = new websocketClient("ws://localhost:9999/graphql");
// require("express-ws")(app);

// This is simply to quiet the console log from complaining about a missing graphql ws
// here we have the ability to intercept the web socket in the future to pipe it through our own endpoint
// app.ws("/graphql", function (ws, req) {
//   ws.on("message", function (msg) {
//     wsClient.on("message", function message(data) {
//       ws.send(data);
//     });
//   });
// });

// app.get("/login", (req, res) => {
//   res.sendFile("dist/login/Login.html", { root: __dirname });
// });

// app.post("/login", bodyParser.urlencoded({ extended: true }), (req, res) => {
//   // console.log(req.body);
//   axios
//     .post("http://localhost:" + config.stash.port + "/login", req.body)
//     .then((d) => console.log("axios has the ball"));
//   // res.send({
//   //   ...req.body,
//   // });
// });

// app.get("/assets/:js", (req, res) => {
//   console.log(req.params.js);
//   const puppeteer = require("puppeteer");
//   const c = require("./config.json");
//   const { protocol, hostname, auth } = c.stash;
//   let stashURL = `${protocol}://${hostname}:${c.stash.port}/assets/${req.params.js}`;
//   (async () => {
//     // const browser = await puppeteer.launch({ headless: false });

//     const browser = await puppeteer.launch({
//       headless: true,
//       ignoreHTTPSErrors: true,
//     });

//     const page = await browser.newPage();
//     await page.goto(stashURL, {
//       waitUntil: "domcontentloaded",
//     });
//     await page.focus("#username");
//     await page.keyboard.type(auth.username);
//     await page.focus("#password");
//     await page.keyboard.type(auth.password);
//     await page.click('[class="btn btn-primary"]');
//     await page.waitForTimeout(1000);

//     await page.goto(stashURL, {
//       waitUntil: "domcontentloaded",
//     });

//     await page.waitForTimeout(1000);

//     let content = await page.content();

//     res.contentType(path.basename(req.params.js));
//     res.send(
//       content
//         .replace(
//           '<html><head><meta name="color-scheme" content="light dark"></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">',
//           ""
//         )
//         .replace("</pre></body></html>", "")
//     );
//     await browser.close();
//   })();
// });

app.use(express.static("dist"));
app.get("/api/plugins", (req, res) => {
  const testFolder = "./dist/plugins/";
  let files = fs.readdirSync(testFolder);
  res.send({ plugins: files });
});

app.get("/network-details", (req, res) => res.send(runningAt()));

app.get("*", (req, res) => res.sendFile(__dirname + "/dist/index.html"));

// const securedHeaders = config.stash.auth
//   ? {
//       headers: {
//         ApiKey: config.stash.auth.apiKey,
//       },
//     }
//   : {};
// PROXY FOR STASH GRAPHQL
app.post("/graphql", bodyParser.json(), (req, res) => {
  const { body } = req;
  axios({
    method: "post",
    url: "http://localhost:" + config.stash.port + "/graphql",
    data: body,
    // ...securedHeaders,
  }).then((d) => {
    res.send(d.data);
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
