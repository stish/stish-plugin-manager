import React, { useEffect } from "react";
import { Plugin } from "stish-react";

Plugin({
  name: "CustomThemeExample",
  description:
    "I am an example of a theme. I make all text orange on the /scenes page",
  settings: {},
  route: "/scenes",
  styles: `
    *{color:orange !important;}
  `,
  component: () => (
    <div
      style={{
        position: "fixed",
        color: "orange",
        bottom: 0,
        left: 10,
        zIndex: 4000,
      }}
    >
      CustomThemeExample
    </div>
  ),
});
