import React from "react";
import { Plugin } from "stish-react";

const bootswatchThemes = [
  "Cyborg",
  "Morph",
  "Pulse",
  "Sandstone",
  "Slate",
  "Solar",
  "Superhero",
  "Vapor",
  "Zephyr",
];

Plugin({
  name: "BootstrapThemes",
  settings: {
    theme: {
      label: "Choose Theme",
      defaultValue: "darkly",
      type: "select",
      options: bootswatchThemes.map((thm) => {
        return {
          label: thm,
          value: thm.toLowerCase(),
        };
      }),
    },
  },
  route: "*",
  component: ({ routing, settings }) => {
    return (
      <div style={{ position: "fixed", top: 70, right: 30, zIndex: 1030 }}>
        {settings?.storage?.theme && (
          <link
            rel="stylesheet"
            href={`https://bootswatch.com/4/${settings?.storage?.theme.toLowerCase()}/bootstrap.min.css`}
          />
        )}
      </div>
    );
  },
});
