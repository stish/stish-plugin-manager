import React, { useEffect, useState } from "react";
import { Plugin, forceEnablePlugin } from "stish-react";

// WARNING: This should not be done in any plugin
// We will automatically enable out plugin when its mounted the first time
forceEnablePlugin("Logineer");

Plugin({
  name: "Logineer",
  settings: {},
  route: "/login",
  component: () => {
    const [username, _username] = useState("");
    const [password, _password] = useState("");
    const login = () => {
      fetch("http://localhost:9999/login", {
        method: "post",
        body: JSON.stringify({
          username,
          password,
          returnURL: "/",
        }),
      })
        .then((d) => d.json())
        .then((data) => console.log(data));
    };
    return (
      <div
        style={{
          width: 300,
          height: 300,
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
          backgroundColor: "white",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          zIndex: 3000,
          color: "white",
          backgroundColor: "black",
          fontFamily: "sans-serif",
          borderRadius: 15,
        }}
      >
        <h1 style={{ margin: 0, padding: 0 }}>Logineer</h1>
        <br />
        <label>
          Username:
          <br />
          <input type="text" onKeyUp={(e) => _username(e.target.value)} />
        </label>
        <br />
        <label>
          Password:
          <br />
          <input type="password" onKeyUp={(e) => _password(e.target.value)} />
        </label>
        <br />
        <button onClick={login}>Login</button>
      </div>
    );
  },
});
