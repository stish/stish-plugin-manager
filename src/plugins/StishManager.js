/*
    This plugin is the official stish plugin used to manage other stish plugins
*/
import React, { useRef, useEffect, useState } from "react";
import styled from "styled-components";
import { Col, Container, Row } from "react-bootstrap";

// import Plugin, { Storage } from "../stish";
import { Plugin, StishStorage as Storage } from "stish-react";

let StishPluginManagerTitle = "Stish Plugin Manager";
const TabPanel = styled.div`
  width: 100%;
  height: 45px;
  overflow: hidden;
  margin-bottom: 15px;
  .title {
    cursor: pointer;
    width: 100%;
    height: 45px;
    display: flex;
    align-items: center;
    padding-left: 20px;
    background-color: #394b59;
    border-radius: 5px;
  }
  &.active {
    height: auto;
    .title {
      font-weight: bold;
    }
  }
`;
const TabContent = styled.div`
  padding: 20px 0px;
  label {
    position: relative;
    top: 4px;
    input {
    }
    .labelText {
      margin-left: 6px;
      position: relative;
      top: -2px;
    }
  }
`;
const PluginsAccordion = ({ items, handleTogglePlugin }) => {
  let [activePanel, _setActivePanel] = useState(0);

  const getSettingsSchemaStored = (pluginName) => {
    let pluginStorageKey = `--plugin-setting-${pluginName}`;
    let findPluginSettingsStored = Storage.getItem(pluginStorageKey);

    if (findPluginSettingsStored && findPluginSettingsStored?.schema) {
      return findPluginSettingsStored.schema;
    } else {
      return {};
    }
  };

  const getSettingKeyValue = (pluginName, settingKey) => {
    let pluginStorageKey = `--plugin-setting-${pluginName}`;
    let findPluginSettingsStored = Storage.getItem(pluginStorageKey);

    if (
      findPluginSettingsStored &&
      findPluginSettingsStored?.storage &&
      findPluginSettingsStored?.storage[settingKey]
    ) {
      return findPluginSettingsStored?.storage[settingKey];
    } else {
      return findPluginSettingsStored?.schema[settingKey]?.defaultValue;
    }
  };

  const setSettingKeyValue = (pluginName, settingKey, settingValue) => {
    let pluginStorageKey = `--plugin-setting-${pluginName}`;
    let findPluginSettingsStored = Storage.getItem(pluginStorageKey);

    let newStoredSettings = findPluginSettingsStored;
    newStoredSettings.storage[settingKey] = settingValue;
    Storage.setItem(pluginStorageKey, newStoredSettings, {
      pluginName,
    });
  };

  return (
    <div>
      {items.length > 0 &&
        items
          .filter((it) => it.title.toLowerCase() !== ".ds_store")
          .map((item, i) => {
            return (
              <TabPanel
                key={`plugin-key-${i}`}
                className={activePanel === i ? "active" : ""}
                onClick={(e) => _setActivePanel(i)}
              >
                <div className="title">
                  {item.title} &nbsp; &nbsp;{" "}
                  <small style={{ color: "orange" }}>
                    ({Storage.getItem("stishMeta")[item.title].category})
                  </small>
                </div>
                <TabContent>
                  <Container fluid>
                    <Row>
                      {Storage.getItem("stishMeta")[item.title].description !==
                        "" && (
                        <Col lg={12}>
                          <small className="mb-3 d-block">
                            {
                              Storage.getItem("stishMeta")[item.title]
                                .description
                            }
                          </small>
                        </Col>
                      )}

                      <Col lg={2}>
                        <label
                          title={
                            item.title === "StishManager"
                              ? "StishManager cannot be disabled because it is required for Stish to work"
                              : ""
                          }
                        >
                          <input
                            disabled={item.title === "StishManager"}
                            type="checkbox"
                            defaultChecked={item.enabled}
                            onChange={(e) =>
                              handleTogglePlugin(item.title, e.target.checked)
                            }
                          />
                          <span className="labelText">Enabled</span>
                        </label>
                      </Col>
                      <Col lg={10}>
                        {Object.keys(getSettingsSchemaStored(item.title)).map(
                          (option, i) => {
                            return (
                              <div key={i}>
                                <label>
                                  {
                                    getSettingsSchemaStored(item.title)[option]
                                      .label
                                  }
                                  : &nbsp;&nbsp;
                                  {getSettingsSchemaStored(item.title)[option]
                                    .type === "select" && (
                                    <select
                                      defaultValue={getSettingKeyValue(
                                        item.title,
                                        option
                                      )}
                                      onChange={(e) => {
                                        setSettingKeyValue(
                                          item.title,
                                          option,
                                          e.target.value
                                        );
                                      }}
                                      className="form-control"
                                      onFocus={(e) => e.stopPropagation()}
                                      onBlur={(e) => e.stopPropagation()}
                                    >
                                      {getSettingsSchemaStored(item.title)[
                                        option
                                      ].options.map((option, i) => (
                                        <option value={option.value} key={i}>
                                          {option.label}
                                        </option>
                                      ))}
                                    </select>
                                  )}
                                  {getSettingsSchemaStored(item.title)[option]
                                    .type === "text" && (
                                    <input
                                      defaultValue={getSettingKeyValue(
                                        item.title,
                                        option
                                      )}
                                      onKeyUp={(e) => {
                                        e.stopPropagation();
                                        setSettingKeyValue(
                                          item.title,
                                          option,
                                          e.target.value
                                        );
                                      }}
                                      className="form-control"
                                      type="text"
                                      onFocus={(e) => e.stopPropagation()}
                                      // onBlur={(e) => e.stopPropagation()}
                                    />
                                  )}
                                </label>
                              </div>
                            );
                          }
                        )}
                      </Col>
                    </Row>
                  </Container>
                  {/* {item.children} */}
                </TabContent>
              </TabPanel>
            );
          })}
    </div>
  );
};

Plugin({
  name: "StishManager",
  keys: [],
  route: "/settings",
  debug_path: true,
  component: ({ routing }) => {
    const [stishSettingsActive, _stishSettingsActive] = useState(false);
    let [panelRect, _setPanelRect] = useState(false);

    const getPanelRect = () =>
      document.getElementsByClassName("tab-content")[0].getBoundingClientRect();

    const [allPlugins, _allPlugins] = useState({});

    useEffect(() => {
      const handleResize = () => {
        // _setPanelRect(getPanelRect());
        setTimeout(function () {
          _setPanelRect(getPanelRect());
        }, 300);
      };

      window.addEventListener("resize", handleResize);

      return () => {
        window.removeEventListener("resize", handleResize);
      };
    }, []);

    useEffect(() => {
      _setPanelRect(getPanelRect());

      fetch("/api/plugins")
        .then((d) => d.json())
        .then((data) => {
          let storedPluginState = Storage.getItem("stishPlugins");

          let newPluginsState = {};
          Object.keys(data.plugins).map(
            (key) =>
              (newPluginsState[data.plugins[key].replace(".js", "")] =
                storedPluginState &&
                storedPluginState[data.plugins[key].replace(".js", "")]
                  ? storedPluginState[data.plugins[key].replace(".js", "")]
                  : false)
          );
          _allPlugins(newPluginsState);
        });
    }, []);

    const togglePlugin = (plugin, checked) => {
      let newPluginsState = { ...allPlugins };
      newPluginsState[plugin] = checked;
      _allPlugins(newPluginsState);
      Storage.setItem("stishPlugins", newPluginsState, {
        pluginName: plugin,
      });

      // if (!checked) {
      //   document.getElementById(`--plugin-${plugin}`).remove();

      //   if (document.getElementById(`--plugin-script-${plugin}`)) {
      //     document.getElementById(`--plugin-script-${plugin}`).remove();
      //   }
      // }
    };

    const resetAllAnchors = () => {
      [...document.getElementsByClassName("nav nav-pills")[0].childNodes]
        .map((c) => c.children[0])
        .filter(function (x) {
          return x !== undefined;
        })
        .map((anchor) => anchor.classList.remove("active"));
    };

    useEffect(() => {
      setTimeout(() => {
        document
          .getElementsByClassName("tab-content")[0]
          .getElementsByClassName("active")[0]
          ?.classList.toggle("active", !stishSettingsActive);

        if (stishSettingsActive) {
          resetAllAnchors();
          document
            .getElementById("stish-settings-tabs-tab-tasks")
            .setAttribute(
              "class",
              `nav-link ${stishSettingsActive ? "active" : ""}`
            );
          document.getElementById("stish-settings-tabs-tab-tasks") &&
            (document.getElementById("stish-settings-tabs-tab-tasks").onclick =
              toggleStishSettings);
        } else {
          // resetAllAnchors();
          document
            .getElementById("stish-settings-tabs-tab-tasks")
            ?.setAttribute("class", `nav-link`);
          document.getElementById("stish-settings-tabs-tab-tasks") &&
            (document.getElementById("stish-settings-tabs-tab-tasks").onclick =
              toggleStishSettings);
        }
      }, 200);
    }, [stishSettingsActive]);

    const toggleStishSettings = () =>
      _stishSettingsActive(!stishSettingsActive);

    const createButton = () => {
      // This is the container that holds all the tabs in the setings page
      let settingsPills = [
        ...document.getElementsByClassName("nav nav-pills"),
      ][0];
      //   console.log(settingsPills);
      let newPillEl = document.createElement("div");
      newPillEl.className = "nav-item";
      let newPillAnchor = document.createElement("a");
      newPillAnchor.setAttribute("role", "tab");
      newPillAnchor.setAttribute("style", "cursor:pointer;");
      newPillAnchor.setAttribute("data-rb-event-key", "stish-settings");
      newPillAnchor.setAttribute("id", "stish-settings-tabs-tab-tasks");
      newPillAnchor.onclick = () => _stishSettingsActive(true);
      newPillAnchor.onblur = () => alert("blurr");
      newPillAnchor.setAttribute(
        "aria-controls",
        "stish-settings-tabs-tabpane-tasks"
      );
      newPillAnchor.setAttribute("aria-selected", "true");
      newPillAnchor.setAttribute(
        "class",
        `nav-link ${stishSettingsActive ? "active" : ""}`
      );
      newPillAnchor.innerText = StishPluginManagerTitle;
      newPillEl.appendChild(newPillAnchor);
      settingsPills?.appendChild(newPillEl);
    };

    useEffect(() => {
      createButton();
      setInterval(() => {
        if ([...document.querySelectorAll(".nav-item > a.active")].length > 1) {
          document
            .getElementById("stish-settings-tabs-tab-tasks")
            .setAttribute("class", "nav-link");
          _stishSettingsActive(false);
        }
      }, 10);
    }, []);

    let bgColor = window
      .getComputedStyle(document.body, null)
      .getPropertyValue("background-color");

    return (
      <div
        onBlur={() => _stishSettingsActive(!stishSettingsActive)}
        style={{
          display: stishSettingsActive ? "block" : "none",
          position: "fixed",
          top: panelRect.top + "px",
          left: panelRect.left + "px",
          color: "white",
          zIndex: 40000,
          // backgroundColor: bgColor,
          height: panelRect.height + "px",
          width: panelRect.width + "px",
          paddingTop: 20,
          paddingLeft: 25,
          paddingRight: 25,
          right: 15,
        }}
      >
        <h1 style={{ display: "inline" }}>{StishPluginManagerTitle}</h1>
        <button className="btn btn-warning float-right">Install</button>
        <button className="btn btn-secondary float-right mr-2">Reload</button>
        <br />
        <br />
        <PluginsAccordion
          handleTogglePlugin={togglePlugin}
          items={Object.keys(allPlugins).map((p) => {
            let enabled = allPlugins[p];
            return {
              title: p,
              enabled,
              children: <p>child test</p>,
            };
          })}
        />
      </div>
    );
  },
});
