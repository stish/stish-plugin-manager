import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom/client";
import { StishLoader, StishStorage } from "stish-react";

setTimeout(() => {
  StishStorage.setItem("stishNavigation", document.location.pathname, {
    type: "ls-stishNavigationUpdate",
  });
  var oldHref = document.location.href;
  window.onload = function () {
    var bodyList = document.querySelector("body");
    var observer = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (oldHref != document.location.href) {
          oldHref = document.location.href;
          /* Changed ! your code here */
          StishStorage.setItem("stishPath", document.location.pathname, {
            type: "ls-stishNavigationUpdate",
          });
        }
      });
    });
    observer.observe(bodyList, {
      childList: true,
      subtree: true,
    });
  };
  const root = ReactDOM.createRoot(
    document.getElementById("stish-ui-container")
  );
  root.render(<StishLoader />);
}, 100);
