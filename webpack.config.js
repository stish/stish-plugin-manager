const path = require("path");
const glob = require("glob");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const config = require("./config.json");

const pluginManager = {
  entry: "./src/entry.js",
  watch: true,
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        // exclude: /node_modules/,
        use: ["babel-loader"],
      },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/static/index.html",
      title: "HTML Webpack Plugin",
      stashRoot: `${config.stash.protocol}://${config.stash.hostname}:${config.stash.port}`,
      inject: false,
      filename: "index.html",
    }),
  ],
};

function getEntries() {
  return fs
    .readdirSync("./src/plugins/")
    .filter((file) => file.match(/.*\.js$/))
    .map((file) => {
      return {
        name: file.substring(0, file.length - 3),
        path: "./src/plugins/" + file,
      };
    })
    .reduce((memo, file) => {
      memo[file.name] = file.path;
      return memo;
    }, {});
}

const plugins = {
  entry: getEntries,
  watch: true,
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, "dist/plugins"),
    filename: "[name].js",
  },
};

module.exports = [pluginManager, plugins];
