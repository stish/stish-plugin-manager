## Update
- The HOC used to create "Stish" plugins is now its own npm pacakge!
  - This was done to abstract the injection and Stish app from the library used to create the actual plugins
  - I plan on releasing a "stish-vue" in the next few months and if i have enough interest in this i might do a "stish-angular".
- The HOC can be useful for other purposes besides plugins for Stish(Stash on Stish)
- The source code for "stish-react" was causing bulk in this project and I felt that it should be maintained on its own.


![Semantic description of image](https://i.ibb.co/f9DJ6T2/Screen-Shot-2022-06-01-at-12-24-37-PM.png)


# Stish - Plugin Manager

Stish allows you to use and create javascript plugins for Stash. Stish...

- plugins are all React components (new)
- works in parallel to Stash on a different port.
- uses the same assets used to render the StashUI but in a seperate server
- pipes the graphql queries through its own server
- allows injecting plugins that are invoked by URL pathnames
- has its own settings page right inside of the StishUI (Stash on new port)
- Provides an HOC (StishReactPlugin) to register and mount your plugin component. StishReacPlugin also...
  - registers flags || keys that can be used to turn your plugin on or off and || or features within your plugin on or off
  - has the ability to execute your plugin based on a specific URL (/performers, /performers/:id, /settings)
  - can provide your plugin with routing info, and current state of the keys (true, false)

## Getting started

### Install Dependencies

`npm install`

### Configure (config.json)

Add your StashUI port, protocol and hostname. This will let Stish know where the javascript bundles are to run the Stish UI

### Run Stash (not stish)

Run Stash as normal, make sure the port does match what you have in the Stish config.json

### Run Dev Server (webpack)

`npm start`

### Open StishUI

`http://localhost:3000`

## Creating a StishPlugin

All plugins can be developed and enhanced inside of the `./src/dev-plugins/*` directory and webpack spits them out in `./dist/plugins/*` as shareable and independent React.js apps.

```javascript
import React, { useEffect } from "react";
import { Plugin } from "stish-react";

Plugin({
    // Title of your plugin, it should match the filename (ex: RandomToggleableButtonsOnPerformersPage.js )
  name: "RandomToggleableButtonsOnPerformersPage",
  // Route where you want your plugin to fire off, it can be a wildcard (/*), a specific page (/performers), or a subpage (/performers/:id)
  route: "/performers",
  // This is where you can wrap your react component.
  // "component" takes in a react component. Use react like normal here
  component: ({ routing, settings }) => {
      let [buttonState, setButtonState] = useState(false);
      return (
          <button
            onClick={()=>setButtonState(!buttonState)}
            style={{
              backgroundColor: buttonState ? 'green' : 'red'
            }
          }>
            I am just a random button on a page
          </button>
      )
  }
);

```

## Creating a theme

Themes use the same HOC as UI react plugins but you can use the "styles" property and pass in CSS, you can also combine a react component and CSS to display a badge for your theme (ex: Pulsar theme by Fonzie)

```javascript
import React, { useEffect } from "react";
import { Plugin } from "stish-react";

Plugin({
  name: "MyCustomTheme",
  route: "*",
  styles: `
    body{
      font-family: Helvetica;
      color: orange;
    }
    h2{
      font-size: 30px;
    }
  `
  component: () => (
        <a
          style={{
            position: "fixed",
            bottom: 0,
            left: 10,
            zIndex: 4000,
          }}
          href="https://mycustomthemewebsite.com"
        >
          My Custom Theme by Me
        </a>
   )
);

```
