const StishPlugin = require("../utils/StishPlugin");

module.exports = () =>
  StishPlugin({
    title: "Testing the new Stish Plugin",
    keys: [],
    route: "/stish-settings",
    handler: ({ keys }) => {
      let stishConfigStored = keys;

      const stishResetButton = document.createElement("button");
      stishResetButton.className = "btn btn-primary stish-reset-button";
      stishResetButton.onclick = () => {
        localStorage.removeItem("stish-config-2");
        window.location.reload();
      };
      stishResetButton.innerHTML = "Reset Keys";

      const onCheckBoxChange = (key, e) => {
        console.log(stishConfigStored, key, e);
        let newConfig = stishConfigStored;
        newConfig[key] = e.target.checked;
        localStorage.setItem("stish-config-2", JSON.stringify(newConfig));
      };

      const newOptionElement = (key) => {
        const optionForm = document.createElement("label");
        optionForm.style =
          "width:100%;display:flex;gap: 10px;height:30px;align-items:center;";
        const checkBox = document.createElement("input");
        const labelText = document.createElement("span");
        labelText.innerHTML = key;
        checkBox.type = "checkbox";
        checkBox.checked = stishConfigStored[key];
        checkBox.onclick = (e) => onCheckBoxChange(key, e);
        optionForm.append(checkBox);
        optionForm.append(labelText);
        console.log(optionForm);
        return optionForm;
      };

      const addConfigOption = (option) => {
        const key = option;
        const defaultValue = stishConfigStored[option];
        document
          .getElementsByClassName("stish-config-container")[0]
          .append(newOptionElement(key));
      };

      document.getElementsByClassName("stish-config-container")[0] &&
        document.getElementsByClassName("stish-config-container")[0].remove();
      document.getElementsByClassName("stish-reset-button")[0] &&
        document.getElementsByClassName("stish-reset-button")[0].remove();

      const settingsContainer = document.createElement("div");
      settingsContainer.className = "stish-config-container";
      document.getElementsByClassName("main")[0].append(settingsContainer);

      document.getElementsByTagName("h1")[0].innerHTML = "Stish Settings";
      Object.keys(stishConfigStored).map((option) => addConfigOption(option));

      document
        .getElementsByClassName("stish-config-container")[0]
        .append(stishResetButton);
    },
  });
