const StishPlugin = require("../utils/StishPlugin");

module.exports = () =>
  StishPlugin({
    title: "Testing the new Stish Plugin",
    keys: [],
    route: "/*",
    handler: ({ keys }) => {
      alert("test");
      const addStishSettingsLink = () => {
        if (!document.getElementsByClassName("stish-link")[0]) {
          const stishLink = document
            .getElementsByClassName("nav-link")[0]
            .cloneNode(true);
          stishLink.className = "stish-link";
          const actionLink = stishLink.firstChild;
          actionLink.getElementsByTagName("svg")[0].remove();
          actionLink.getElementsByTagName("span")[0].innerHTML = "Stish";
          actionLink.href = "/stish-settings";
          document.getElementsByClassName("navbar-nav")[0].append(stishLink);
        }
      };

      addStishSettingsLink();
    },
  });
