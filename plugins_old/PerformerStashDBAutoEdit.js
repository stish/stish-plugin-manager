const StishPlugin = require("../utils/StishPlugin");

module.exports = () =>
  StishPlugin({
    title: "PerformerStashDBAutoEdit",
    keys: [
      "autoOpenPerformerEditing",
      "autoScrapeStashDBImagesForPerformers",
      "autoScrapeStashDBImagesForPerformers-infinite",
    ],
    route: "/performers/:id",
    handler: ({ keys, routing }) => {
      let browser_id = routing.browser_route.id;

      document.onkeydown = checkKey;

      function checkKey(e) {
        e = e || window.event;

        if (e.keyCode == "38") {
          // up arrow
        } else if (e.keyCode == "40") {
          // down arrow
        } else if (e.keyCode == "37") {
          // left arrow
          window.location.href = `/performers/${Number(browser_id) - 1}`;
        } else if (e.keyCode == "39") {
          // right arrow
          window.location.href = `/performers/${Number(browser_id) + 1}`;
        }
      }

      if (keys.autoOpenPerformerEditing) {
        let editCancelButton =
          document.getElementsByClassName("details-edit")[0].children[0];

        if (editCancelButton.innerHTML === "Edit") {
          editCancelButton.click();

          if (keys.autoScrapeStashDBImagesForPerformers) {
            document.getElementsByClassName("dropup")[0].children[0].click();
            document
              .getElementById("performer-scraper-popover")
              .children[0].click();
            setTimeout(() => {
              document
                .getElementsByClassName("PerformerScrapeModal-list")[0]
                .children[0].children[0].click();
            }, 200);

            setTimeout(() => {
              document
                .getElementsByClassName("ModalFooter")[0]
                .children[1].children[1].click();
            }, 300);
            setTimeout(() => {
              document
                .getElementsByClassName("details-edit")[0]
                .children[4].click();
            }, 800);
            setTimeout(() => {
              if (keys["autoScrapeStashDBImagesForPerformers-infinite"]) {
                window.location.href = `/performers/${Number(browser_id) + 1}`;
              }
            }, 1200);
          }
        }
      }
    },
  });
