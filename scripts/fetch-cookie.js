/*
Secured Stash Setup

If your stash instance is password protected this causes issues with the way that Stish works. This script will attempt to:
- Login to your stash instance, and generate an API key, and store API key in ./stash-api-key.json

- 
*/

const fs = require("fs");
const puppeteer = require("puppeteer");

(async () => {
  const c = require("../config.json");
  const { protocol, hostname, port, auth } = c.stash;
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(`${protocol}://${hostname}:${port}`);

  await page.focus("#username");
  await page.keyboard.type(auth.username);
  await page.focus("#password");
  await page.keyboard.type(auth.password);
  await page.click('[class="btn btn-primary"]');
  const cookies = await page.cookies();

  setTimeout(async () => {
    console.log("cookies", cookies);
    await browser.close();
  }, 60000 * 4);

  //   await browser.close();
})();
