const puppeteer = require("puppeteer");
const path = require("path");
const { URL } = require("url");
const fse = require("fs-extra"); // v 5.0.0
const { protocol, hostname, port } = require("../config.json").stash;

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  /* 2 */
  page.on("response", async (response) => {
    const url = new URL(response.url());
    let filePath = path.resolve(`./tmp/${url.pathname}`);
    console.log(url.pathname);
    if (path.extname(url.pathname).trim() === "") {
      filePath = `${filePath}/index.html`;
    }
    await fse.outputFile(filePath, await response.buffer());
  });

  /* 3 */
  await page.goto(`http://localhost:9999`, {
    waitUntil: "networkidle2",
  });

  /* 4 */
  setTimeout(async () => {
    await browser.close();
  }, 60000 * 4);
})();
