const express = require("express");
const app = express();
const port = 4000;
const puppeteer = require("puppeteer");
const c = require("../config.json");
const { protocol, hostname, auth } = c.stash;
let stashURL = `${protocol}://${hostname}:${c.stash.port}/login`;

require("express-ws")(app);

app.ws("/echo", function (ws, req) {
  ws.on("message", function (msg) {
    console.log(msg);
    ws.send(msg);
  });
});

app.get("/client-test", (req, res) => {
  res.send(`
    <script type="text/javascript">
     let url = 'ws://localhost:4000/echo';
     webSocket = new WebSocket(url);


     webSocket.onopen = function (event) {
        webSocket.send("Here's some text that the server is urgently awaiting!");
      };

      webSocket.onmessage = function(event) {
        console.log("[message] Data received from server:" + event.data);
      };

      const sendValue = v => webSocket.send(v);




    
    </script>
 <input type="text" onChange="sendValue(this.value)" />
     
     `);
});

app.get("/stash-ui", (req, res) => {
  (async () => {
    // const browser = await puppeteer.launch({ headless: false });

    const browser = await puppeteer.launch({
      headless: false,
      ignoreHTTPSErrors: true,
      //   args: [`--window-size=1920,1080`],
      //   defaultViewport: {
      //     width: 1920,
      //     height: 1080,
      //   },
    });

    const page = await browser.newPage();
    await page.goto(stashURL, {
      waitUntil: "domcontentloaded",
    });
    await page.focus("#username");
    await page.keyboard.type(auth.username);
    await page.focus("#password");
    await page.keyboard.type(auth.password);
    await page.click('[class="btn btn-primary"]');
    await page.waitForTimeout(1000);
    let content = await page.content();
    const script = require.resolve("./security-listener.js");
    await page.addScriptTag({ path: script });
    await page.addScriptTag({
      path: "https://unpkg.com/peerjs@1.4.5/dist/peerjs.min.js",
    });
    await page.waitForTimeout(1000);
    res.send(`
   

       <h1>a</h1>
    
    `);
  })();
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
